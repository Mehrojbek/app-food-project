package ai.ecma.appfastfood.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CountSale {

    private Integer id;

    private Integer saleId;

    private Integer productId;

    private Integer productCount;

    private Integer presentCount;

}
