package ai.ecma.appfastfood.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PriceSale {

    private Integer id;

    private Integer saleId;

    private Double price;

    private Double saleAmount;

}
