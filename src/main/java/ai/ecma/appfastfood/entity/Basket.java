package ai.ecma.appfastfood.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Basket {

    private Integer id;

    private String fullName;
    private String phoneNumber;
    private Double price;
    private Integer saleId;
    private Double finalPrice;
}
