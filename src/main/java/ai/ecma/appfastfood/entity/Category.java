package ai.ecma.appfastfood.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Category {

    private Integer id;

    @NotNull(message = "Nom berilishi majburiy")
    @NotBlank(message = "Nom berilishi majburiy")
    private String name;

    private String description;

}
