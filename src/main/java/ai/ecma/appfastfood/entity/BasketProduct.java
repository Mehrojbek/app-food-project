package ai.ecma.appfastfood.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class BasketProduct {

    private Integer id;

    private Integer basketId;

    private Integer productId;

    private Integer count;
}
