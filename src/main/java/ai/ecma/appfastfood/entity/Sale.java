package ai.ecma.appfastfood.entity;

import ai.ecma.appfastfood.entity.enums.SaleTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor@NoArgsConstructor
@Data
public class Sale {

    private Integer id;

    private String name;

    private SaleTypeEnum type;

    private String description;
}
