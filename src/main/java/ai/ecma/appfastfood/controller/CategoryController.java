package ai.ecma.appfastfood.controller;

import ai.ecma.appfastfood.entity.Category;
import ai.ecma.appfastfood.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/category")
@RequiredArgsConstructor
public class CategoryController {
    private final CategoryService categoryService;

    @GetMapping("/form")
    public String getForm(Model model){
        return categoryService.getForm(model);
    }

    @PostMapping("/add")
    public String addCategory(Model model, @ModelAttribute @Valid Category category){
        return categoryService.addCategory(model,category);
    }

}
