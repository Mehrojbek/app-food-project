package ai.ecma.appfastfood.controller;

import ai.ecma.appfastfood.entity.Category;
import ai.ecma.appfastfood.entity.Product;
import ai.ecma.appfastfood.service.CategoryService;
import ai.ecma.appfastfood.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/product")
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;

    @GetMapping("/form")
    public String getForm(Model model) {
        return productService.getForm(model);
    }

    @PostMapping("/add")
    public String addCategory(Model model, @ModelAttribute @Valid Product product) {
        return productService.add(model, product);
    }

}
