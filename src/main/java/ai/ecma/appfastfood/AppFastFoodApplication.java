package ai.ecma.appfastfood;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppFastFoodApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppFastFoodApplication.class, args);
    }

}
