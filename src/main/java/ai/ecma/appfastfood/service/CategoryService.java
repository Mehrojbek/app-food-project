package ai.ecma.appfastfood.service;

import ai.ecma.appfastfood.entity.Category;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CategoryService {
    private final DbService dbService;
    public String getForm(Model model) {
        List<Category> categoryList = dbService.getCategories();
        model.addAttribute("categoryList",categoryList);
        return "categoryForm";
    }

    public String addCategory(Model model, Category category) {
        boolean exist = dbService.existCategory(category.getName());
        if (exist) return "categoryForm";
        List<Category> categoryList = dbService.createCategory(category);
        model.addAttribute("categoryList",categoryList);
        return "categoryForm";
    }
}
