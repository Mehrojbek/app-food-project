package ai.ecma.appfastfood.service;

import ai.ecma.appfastfood.entity.Category;
import ai.ecma.appfastfood.entity.Product;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final DbService dbService;
    public String getForm(Model model) {
        List<Category> categoryList = dbService.getCategories();
        List<Product> productList = dbService.getProducts();

        model.addAttribute("categoryList",categoryList);
        model.addAttribute("productList",productList);

        return "productForm";
    }

    public String add(Model model, Product product) {
//        boolean exist = dbService.existProduct(product.getName());
//        if (exist) return "productForm";
        dbService.createProduct(product);
        return getForm(model);
    }
}
