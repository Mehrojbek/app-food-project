package ai.ecma.appfastfood.service;

import ai.ecma.appfastfood.entity.Category;
import ai.ecma.appfastfood.entity.Product;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class DbService {

    private final String url = "jdbc:postgresql://localhost:5432/fast_food";
    private final String user = "postgres";
    private final String password = "root123";


    public Connection getConnection() {
        try {
            Class.forName("org.postgresql.Driver");
            return DriverManager.getConnection(url, user, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new RuntimeException();
    }

    public List<Category> getCategories() {

        List<Category> categoryList = new ArrayList<>();
        try(Connection connection = getConnection()) {

            Statement statement = connection.createStatement();
            String query = "select * from category";
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                Category category = new Category(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("description")
                );
                categoryList.add(category);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return categoryList;
    }

    public List<Product> getProducts() {

        List<Product> productList = new ArrayList<>();
        try(Connection connection = getConnection()) {

            Statement statement = connection.createStatement();
//            String query = "select p.id p_id, p.name p_name, p.description p_desc, p.price p_price, p.category_id p_category_id," +
//                    "p.photo p_photo, c.id c_id  from product p join category c on c.id=p.category_id";
            String query = "select * from product p join category c on c.id=p.category_id";
            ResultSet resultSet = statement.executeQuery(query);

            int columnCount = resultSet.getMetaData().getColumnCount();
            for (int i = 1; i <= columnCount; i++) {
//                System.out.print(resultSet.getString(i));
                System.out.println(resultSet.getMetaData().getColumnName(i));
            }
            while (resultSet.next()) {

                Product product = new Product(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getDouble(4),
                        resultSet.getInt(5),
                        resultSet.getString(6),
                        new Category(
                                resultSet.getInt(7),
                                resultSet.getString(8),
                                resultSet.getString(9)
                        )
                );
                productList.add(product);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return productList;
    }

    public boolean existCategory(String name) {
        try(Connection connection = getConnection()) {

            String query = "select * from category where name=?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1,name);
            ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Category> createCategory(Category category) {
        try(Connection connection = getConnection()) {

            String query = "insert into category(name,description) values (?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1,category.getName());
            preparedStatement.setString(2,category.getDescription());
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return getCategories();
    }



    public List<Product> createProduct(Product product) {
        try(Connection connection = getConnection()) {

            String query = "insert into product(name,description,price,category_id) values (?,?,?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1,product.getName());
            preparedStatement.setString(2,product.getDescription());
            preparedStatement.setDouble(3,product.getPrice());
            preparedStatement.setInt(4,product.getCategoryId());
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return getProducts();
    }
}
